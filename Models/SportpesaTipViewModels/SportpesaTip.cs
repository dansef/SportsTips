using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace SportsTips.Models.SportpesaTipViewModels
{
    public class SportpesaTip
    {
        public int ID { get; set; }

        [StringLength(150, MinimumLength = 1)]
        public string TeamA { get; set; }

        [StringLength(150, MinimumLength = 1)]
        public string TeamB { get; set; }

        [StringLength(150, MinimumLength = 1)]
        public string League { get; set; }

        [StringLength(150, MinimumLength = 1)]
        public string TheTip { get; set; }

        public double Odd { get; set; }

        public bool IsPlayed { get; set; }

        public bool IsFriendlyMatch { get; set; }

        public Nullable<bool> IsOddCorrect { get; set; }

        public string HalfTimeResults { get; set; }

        public string FullTimeResults { get; set; }

        //[TimeStamp]
        //public byte[] RowVersion { get; set; }

        [Display(Name = "Day of the Match")]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime? StartDate { get; set; }

        [Display(Name = "Kick-off")]
        [DataType(DataType.Time)]
        public DateTime KickOff { get; set; }

        public string TeamAImageUrl { get; set; }

        public string TeamBImageUrl { get; set; }
        
        public string MatchStatus { get; set; }
    }
}
