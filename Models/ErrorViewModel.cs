using System;

namespace SportsTips.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }

        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}

//$cert = New-SelfSignedCertificate -Subject localhost -DnsName localhost -FriendlyName "SportsTips Certificate" -KeyUsage DigitalSignature -TextExtension @("2.5.29.37={text}1.3.6.1.5.5.7.3.1") 
//Export-Certificate -Cert $cert -FilePath cert.cer
//Import-Certificate -FilePath cert.cer -CertStoreLocation cert:/CurrentUser/Root