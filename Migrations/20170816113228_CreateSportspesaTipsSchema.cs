﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace SportsTips.Migrations
{
    public partial class CreateSportspesaTipsSchema : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TblSportpesaTip",
                columns: table => new
                {
                    ID = table.Column<int>(type: "INTEGER", nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    FullTimeResults = table.Column<string>(type: "TEXT", nullable: true),
                    HalfTimeResults = table.Column<string>(type: "TEXT", nullable: true),
                    IsFriendlyMatch = table.Column<bool>(type: "INTEGER", nullable: false),
                    IsPlayed = table.Column<bool>(type: "INTEGER", nullable: false),
                    KickOff = table.Column<DateTime>(type: "TEXT", nullable: false),
                    League = table.Column<string>(type: "TEXT", maxLength: 150, nullable: true),
                    MatchStatus = table.Column<string>(type: "TEXT", nullable: true),
                    Odd = table.Column<double>(type: "REAL", nullable: false),
                    StartDate = table.Column<DateTime>(type: "TEXT", nullable: true),
                    TeamA = table.Column<string>(type: "TEXT", maxLength: 150, nullable: true),
                    TeamAImageUrl = table.Column<string>(type: "TEXT", nullable: true),
                    TeamB = table.Column<string>(type: "TEXT", maxLength: 150, nullable: true),
                    TeamBImageUrl = table.Column<string>(type: "TEXT", nullable: true),
                    TheTip = table.Column<string>(type: "TEXT", maxLength: 150, nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TblSportpesaTip", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TblSportpesaTip");
        }
    }
}
