using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsTips.Models.SportpesaTipViewModels;
using SportsTips.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace SportsTips.Controllers
{
    [Produces("application/json")]
    [Route("api/Tips")]
    public class TipsController : Controller
    {
        private readonly SportsTipsDbContext _context;

        public TipsController(SportsTipsDbContext context)
        {
            _context = context;
        }

        // GET: api/Tips
        [HttpGet]
        public IEnumerable<SportpesaTip> GetTips()
        {
            return _context.SportpesaTips;
        }

        // GET: api/Tips/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetTip([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tip = await _context.SportpesaTips.SingleOrDefaultAsync(m => m.ID == id);

            if (tip == null)
            {
                return NotFound();
            }

            return Ok(tip);
        }

        // PUT: api/Tips/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutTip([FromRoute] int id, [FromBody] SportpesaTip tip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tip.ID)
            {
                return BadRequest();
            }

            _context.Entry(tip).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TipExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Tips
        [HttpPost]
        public async Task<IActionResult> PostTip([FromBody] SportpesaTip tip)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _context.SportpesaTips.Add(tip);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (TipExists(tip.ID))
                {
                    return new StatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetTip", new { id = tip.ID }, tip);
        }

        // DELETE: api/Tips/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteTip([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var tip = await _context.SportpesaTips.SingleOrDefaultAsync(m => m.ID == id);
            if (tip == null)
            {
                return NotFound();
            }

            _context.SportpesaTips.Remove(tip);
            await _context.SaveChangesAsync();

            return Ok(tip);
        }

        private bool TipExists(int id)
        {
            return _context.SportpesaTips.Any(e => e.ID == id);
        }
    }
}