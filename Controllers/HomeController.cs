﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SportsTips.Models;
using SportsTips.Data;
using SportsTips.Models.SportpesaTipViewModels;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace SportsTips.Controllers
{
    public class HomeController : Controller
    {
        private readonly SportsTipsDbContext _context;

        public HomeController(SportsTipsDbContext context)
        {
            _context = context;    
        }

        public IActionResult Indexx()
        {
            return View();
        }

        // GET: BettingTips/ManageBettingTips
        public async Task<IActionResult> Index(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewData["CurrentSort"] = sortOrder;

            ViewData["TeamASortParm"] = String.IsNullOrEmpty(sortOrder) ? "teamA" : "";
            ViewData["TeamBSortParm"] = sortOrder == "TeamA" ? "teamA_desc" : "TeamA";
            ViewData["LeagueSortParm"] = sortOrder == "League" ? "league_desc" : "League";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;



            var tips = from t in _context.SportpesaTips.Where(t => t.IsPlayed != true) select t;
            //var tips = _tips.OrderByDescending(t => t.ID);

            if (!String.IsNullOrEmpty(searchString))
            {
                tips = tips.Where(s => s.TeamA.Contains(searchString)
                                       || s.TeamB.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "teamA":
                    tips = tips.OrderByDescending(t => t.TeamA);
                    break;
                case "TeamB":
                    tips = tips.OrderBy(t => t.TeamB);
                    break;
                case "teamB_desc":
                    tips = tips.OrderByDescending(t => t.TeamB);
                    break;
                case "League":
                    tips = tips.OrderBy(t => t.League);
                    break;
                case "league_desc":
                    tips = tips.OrderByDescending(t => t.League);
                    break;
                default:
                    tips = tips.OrderBy(t => t.TeamA);
                    break;
            }

            int pageSize = 5;

            //);
            //return View(await _context.AddTrainingViewModels.ToListAsync());
            return View(await PaginatedList<SportpesaTip>.CreateAsync(tips.AsNoTracking(), page ?? 1, pageSize));                      
        }

        public async Task<IActionResult> AdminIndex(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewData["CurrentSort"] = sortOrder;

            ViewData["TeamASortParm"] = String.IsNullOrEmpty(sortOrder) ? "teamA" : "";
            ViewData["TeamBSortParm"] = sortOrder == "TeamA" ? "teamA_desc" : "TeamA";
            ViewData["LeagueSortParm"] = sortOrder == "League" ? "league_desc" : "League";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;



            var tips = from t in _context.SportpesaTips.OrderByDescending(t => t.ID) select t;

            if (!String.IsNullOrEmpty(searchString))
            {
                tips = tips.Where(s => s.TeamA.Contains(searchString)
                                       || s.TeamB.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "teamA":
                    tips = tips.OrderByDescending(t => t.TeamA);
                    break;
                case "TeamB":
                    tips = tips.OrderBy(t => t.TeamB);
                    break;
                case "teamB_desc":
                    tips = tips.OrderByDescending(t => t.TeamB);
                    break;
                case "League":
                    tips = tips.OrderBy(t => t.League);
                    break;
                case "league_desc":
                    tips = tips.OrderByDescending(t => t.League);
                    break;
                default:
                    tips = tips.OrderBy(t => t.TeamA);
                    break;
            }

            int pageSize = 5;

            //);
            //return View(await _context.AddTrainingViewModels.ToListAsync());
            return View(await PaginatedList<SportpesaTip>.CreateAsync(tips.AsNoTracking(), page ?? 1, pageSize));                      
        }

        // GET: BettingTips/ManageBettingTips/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tip = await _context.SportpesaTips
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tip == null)
            {
                return NotFound();
            }

            return View(tip);
        }

        // GET: Home/AdminDetails/5
        public async Task<IActionResult> AdminDetails(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tip = await _context.SportpesaTips
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tip == null)
            {
                return NotFound();
            }

            return View(tip);
        }

        // GET: BettingTips/ManageBettingTips/Create
        public IActionResult AdminCreate()
        {
            return View();
        }

        // POST: BettingTips/ManageBettingTips/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminCreate([Bind("ID,TeamA,TeamB,League,StartDate,KickOff,TheTip,Odd,IsPlayed,IsFriendlyMatch,MatchStatus,HalfTimeResults,FullTimeResults,TeamAImageUrl,TeamBImageUrl")] SportpesaTip tip)
        {
            if (ModelState.IsValid)
            {
                _context.Add(tip);
                await _context.SaveChangesAsync();
                return RedirectToAction("AdminIndex");
                //return RedirectToAction("AdminCreate");
            }
            return View(tip);
        }

        // GET: BettingTips/ManageBettingTips/Edit/5
        public async Task<IActionResult> AdminEdit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tip = await _context.SportpesaTips.SingleOrDefaultAsync(m => m.ID == id);
            if (tip == null)
            {
                return NotFound();
            }
            return View(tip);
        }

        // POST: BettingTips/ManageBettingTips/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> AdminEdit(int id, [Bind("ID,TeamA,TeamB,League,StartDate,KickOff,TheTip,Odd,IsPlayed,IsFriendlyMatch,MatchStatus,HalfTimeResults,FullTimeResults,TeamAImageUrl,TeamBImageUrl")] SportpesaTip tip)
        {
            if (id != tip.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(tip);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SportpesaTipExists(tip.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction("AdminIndex");
            }
            return View(tip);
        }

        // GET: BettingTips/ManageBettingTips/Delete/5
        public async Task<IActionResult> AdminDelete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var tip = await _context.SportpesaTips
                .SingleOrDefaultAsync(m => m.ID == id);
            if (tip == null)
            {
                return NotFound();
            }

            return View(tip);
        }

        // POST: BettingTips/ManageBettingTips/Delete/5
        [HttpPost, ActionName("AdminDelete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var tip = await _context.SportpesaTips.SingleOrDefaultAsync(m => m.ID == id);
            _context.SportpesaTips.Remove(tip);
            await _context.SaveChangesAsync();
            return RedirectToAction("AdminIndex");
        }

        public async Task<IActionResult> Archive(string sortOrder, string currentFilter, string searchString, int? page)
        {

            ViewData["CurrentSort"] = sortOrder;

            ViewData["TeamASortParm"] = String.IsNullOrEmpty(sortOrder) ? "teamA" : "";
            ViewData["TeamBSortParm"] = sortOrder == "TeamA" ? "teamA_desc" : "TeamA";
            ViewData["LeagueSortParm"] = sortOrder == "League" ? "league_desc" : "League";

            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            ViewData["CurrentFilter"] = searchString;



            var tips = from t in _context.SportpesaTips select t;
            //var tips = _tips.OrderByDescending(t => t.ID);

            if (!String.IsNullOrEmpty(searchString))
            {
                tips = tips.Where(s => s.TeamA.Contains(searchString)
                                       || s.TeamB.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "teamA":
                    tips = tips.OrderByDescending(t => t.TeamA);
                    break;
                case "TeamB":
                    tips = tips.OrderBy(t => t.TeamB);
                    break;
                case "teamB_desc":
                    tips = tips.OrderByDescending(t => t.TeamB);
                    break;
                case "League":
                    tips = tips.OrderBy(t => t.League);
                    break;
                case "league_desc":
                    tips = tips.OrderByDescending(t => t.League);
                    break;
                default:
                    tips = tips.OrderBy(t => t.TeamA);
                    break;
            }

            int pageSize = 500;

            //);
            //return View(await _context.AddTrainingViewModels.ToListAsync());
            return View(await PaginatedList<SportpesaTip>.CreateAsync(tips.AsNoTracking(), page ?? 1, pageSize));                      
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        private bool SportpesaTipExists(int id)
        {
            return _context.SportpesaTips.Any(e => e.ID == id);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
