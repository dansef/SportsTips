using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SportsTips.Models;
using SportsTips.Models.SportpesaTipViewModels;

namespace SportsTips.Data
{
    public class SportsTipsDbContext : DbContext
    {
        public SportsTipsDbContext(DbContextOptions<SportsTipsDbContext> options) : base(options)
        { }
        
        public DbSet<SportpesaTip> SportpesaTips { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // base.OnModelCreating(modelBuilder);
            // Customize the ASP.NET Identity model and override the defaults if needed.
            // For example, you can rename the ASP.NET Identity table names and more.
            // Add your customizations after calling base.OnModelCreating(builder);

            modelBuilder.Entity<SportpesaTip>().ToTable("TblSportpesaTip");

            //modelBuilder.Entity<SportpesaTip>().Property(p => p.RowVersion).IsConcurrencyToken();
        }
    }
}
